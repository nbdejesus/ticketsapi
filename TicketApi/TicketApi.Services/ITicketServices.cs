﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TicketApi.Entities;
using TicketApi.Services.ViewModels;

namespace TicketApi.Services
{
   public interface ITicketServices
    {
        Task<IEnumerable<TickectViewModels>> GetTicket();
        Task<TickectViewModels> GetTicket(int id);
        Task<Tickets> UpdateTicket(TickectViewModels model);
        Task<Tickets> AddTicket(TickectViewModels model);
        Task<Tickets> Desactivar(int id);
        Task<Tickets> Activo(int id);
    }
}
