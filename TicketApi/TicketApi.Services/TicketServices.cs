﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketApi.Entities;
using TicketApi.Services.ViewModels;

namespace TicketApi.Services
{
    public class TicketServices : ITicketServices
    {
        private readonly ApplicationDBContext _dbContext;
        public TicketServices(ApplicationDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public async Task<Tickets> AddTicket(TickectViewModels model)
        {
            Tickets  ticket = new Tickets
            {
                
                 Nombre = model.Nombre,
                 CreateDate = DateTime.Now,
                 UpdateDate = DateTime.Now,
                 Estatus = model.Estatus,
                 Usuario = model.Usuario,
            };
            try
            {
                _dbContext.Tickets.Add(ticket);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception)
            {

            }
            return ticket;



        }

        public async Task<TickectViewModels> GetTicket(int id)
        {
            var tickets = await _dbContext.Tickets.FindAsync(id);

            TickectViewModels response = new TickectViewModels
            {
                Id          = tickets.Id,
                Nombre      = tickets.Nombre,
                Usuario     = tickets.Usuario,
                 CreateDate = tickets.CreateDate,
                 UpdateDate = tickets.UpdateDate,
                 Estatus    = tickets.Estatus,                
            };
            return response;

        }

        public async Task<IEnumerable<TickectViewModels>> GetTicket()
        {
            var ticket = await _dbContext.Tickets.ToListAsync();

            return ticket.Select(c => new TickectViewModels
            {
                Id = c.Id,
                 Usuario = c.Usuario,
                 Nombre = c.Nombre,
                 CreateDate = c.CreateDate,
                 UpdateDate = c.UpdateDate,
                 Estatus = c.Estatus
            });

        }

        public async Task<Tickets> UpdateTicket(TickectViewModels model)
        {
            var tickets = await _dbContext.Tickets.FirstOrDefaultAsync(a => a.Id == model.Id);

            if (tickets == null)
            {
                return null;
            }
            tickets.Id = model.Id;
            tickets.Nombre = model.Nombre;
            tickets.Usuario = model.Usuario;            
            tickets.UpdateDate = DateTime.Now;
            tickets.Estatus = model.Estatus;

            try
            {
                _dbContext.Update(tickets);
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción

            }
            return tickets;

        }

        public async Task<Tickets> Desactivar(int id)
        {
            var tickets = await _dbContext.Tickets.FirstOrDefaultAsync(a => a.Id == id);

            if (tickets == null)
            {
                return null;
            }           
            tickets.Estatus = "cerrado";

            try
            {
                _dbContext.Update(tickets);
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción

            }
            return tickets;

        }

        public async Task<Tickets> Activo(int id)
        {
            var tickets = await _dbContext.Tickets.FirstOrDefaultAsync(a => a.Id == id);

            if (tickets == null)
            {
                return null;
            }
            tickets.Estatus = "abierto";

            try
            {
                _dbContext.Update(tickets);
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción

            }
            return tickets;

        }

    }
}
