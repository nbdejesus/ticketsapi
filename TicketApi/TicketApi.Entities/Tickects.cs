﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicketApi.Entities
{
    public class Tickets
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Estatus { get; set; }
    }
}
