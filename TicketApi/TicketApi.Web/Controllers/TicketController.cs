﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TicketApi.Services;
using TicketApi.Services.ViewModels;

namespace TicketApi.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketServices  _ticketServices;

        public TicketController(ITicketServices ticketServices)
        {
            _ticketServices = ticketServices;
        }

        // GET: api/<TicketController>
        [HttpGet]
        public async Task<IEnumerable<TickectViewModels>> Get()
        {
            var ticket = await _ticketServices.GetTicket();
            return ticket;
        }

        // GET api/<TicketController>/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ticket = await _ticketServices.GetTicket(id);
            if (ticket == null)
            {
                return NotFound();
            }

            return Ok(ticket);
        }

        // POST api/<TicketController>
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] TickectViewModels model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }            
            try
            {
                await _ticketServices.AddTicket(model);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();

        }

        // PUT api/<TicketController>/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] TickectViewModels model)
        {
            if (!ModelState.IsValid || model.Id <= 0)
            {
                return BadRequest(ModelState);
            }         
                       
            try
            {
                await _ticketServices.UpdateTicket(model);
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }
            return Ok();

        }

        // DELETE api/<TicketController>/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }           

            try
            {
                 await _ticketServices.Activo(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();


        }

        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            try
            {
                await _ticketServices.Desactivar(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();


        }


    }
}
